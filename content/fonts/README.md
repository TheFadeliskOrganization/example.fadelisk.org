
FONTS
=====

Add your fonts here. You can then add them to your site config and
serve them directly from your server.  If you use the default head
(head.head()), locally served fonts will be inserted into the page
head for you. Or, if you layout your own page head, you can insert
them using head.local_fonts().


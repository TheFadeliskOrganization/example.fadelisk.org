
JAVASCRIPT
==========

Add your javascript scripts here. You can then add them to your site config
this way:

    scripts:
	- '/javascript/myscript.js'
	- '/javascript/site.js'

If you use the default head (head.head()), scripts will be inserted into the
page head for you. Or, if you layout your own page head, you can insert them
using head.scripts().


<%namespace name="document"       file="/document.mako"  />
<%namespace name="head"           file="/head.mako"      />
<%namespace name="title"          file="/title.mako"     />
<%namespace name="menu"           file="/menu.mako"      />
<%namespace name="page_exception" file="/exception.mako" />
<%namespace name="copyright"      file="/copyright.mako" />
<%namespace name="debug"          file="/debug.mako"     />

<%
    content = page_exception.catch(next.body)
%>

<%document:document_html5>
    ${head.head()}
    <body>
        ## Header
        <header>
            <a href="/">
                ${site.conf.get('site_name', 'Example Fadelisk Site')}
            </a>
        </header>

        ## Navigation
        ${menu.nav(items=[
            '/about_this_site/',
            '/docs/',
        ])}

        ## Content
        <section class="content">
        % if not request_data['flags'].get('no_title_in_layout'):
                <h1>${title.title()}</h1>
            % endif
            ${content}
        </section>

        ## Footer
        <footer>
            ${copyright.copyright_declaration()}
        </footer>
    </body>
</%document:document_html5>



EXAMPLE FADELISK SITE
=====================

Clone this project to /srv/www/sites:

    mkdir -p /srv/www/sites
    cd /srv/www/sites
    git clone https://github.com/TheFadeliskOrganization/example.fadelisk.org.git

Alternatively, download the archive and unpack it into /var/www/sites:

    wget https://github.com/TheFadeliskOrganization/example.fadelisk.org/archive/master.tar.gz
    tar xf master.tar.gz
    cp -a example.fadelisk.org-master /srv/www/sites/example.fadelisk.org

Restart fadelisk:

   fadelisk stop
   fadelisk start

Then browse http://example.fadelisk.org:1066/

